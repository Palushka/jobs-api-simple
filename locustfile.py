from locust import HttpLocust, HttpUser, TaskSet, task, between

# init example from tutorial https://microsoft.github.io/PartsUnlimitedMRP/pandp/200.1x-PandP-LocustTest.html


class UserBehavior(TaskSet):

    @task
    def get_tests(self):
        self.client.get("/tests")

    @task
    def put_tests(self):
        self.client.post("/tests", {
            "name": "load testing",
            "description": "checking if a software can handle the expected load"
        })


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(3, 5)


class QuickstartUser(HttpUser):
    wait_time = between(1, 2)

    # @task
    # def index_page(self):
    #     self.client.get("/")
    @task
    def show_vacancies(self):
        self.client.get("/vacancy/?title=java&pub_date=2019-04", name="/vacancy")