from fastapi import FastAPI, Request, Response
import pymongo
import re
import json
from fastapi.templating import Jinja2Templates

app = FastAPI()
templates = Jinja2Templates(directory="templates")

MONGO_DB = 'emise-course'
# student read-only access credentials:
MONGO_URI = 'mongodb://student:X60z0OecRnosJQii@ds113455.mlab.com:13455/emise-course'
client = pymongo.MongoClient(MONGO_URI)
db = client[MONGO_DB]
collection = db['vacancies']
date = '2019-03-'
datePattern = re.compile(r'' + date + '[0-9]{2}')
title = 'python developer'
titlePattern = re.compile(r'' + title, re.I)

vacancies = collection.find({'pub_date': datePattern, 'title': titlePattern}, {'_id': False})

vac_decode = list(vacancies)
for i in vac_decode:
    for x in i:
        i[x] = [re.sub(r'\\xa0', ' ', str(i[x]))[2:-2]]

temp = json.dumps(vac_decode, indent=2)
result = json.loads(temp)


@app.get("/vacancies")
async def root(request: Request):
    return templates.TemplateResponse('vacancies.html', {'request': request,
                                                         'vacancies': result})


@app.get("/")
async def root(request: Request):
    return templates.TemplateResponse('index.html', {'request': request})
