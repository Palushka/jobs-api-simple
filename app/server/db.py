import motor.motor_asyncio
from bson.objectid import ObjectId
from decouple import config
import re

MONGO_URI = f"mongodb://{config('MONGODB_LOGIN')}:{config('MONGODB_PASS')}@" \
            f"{config('MONGODB_HOST')}:{config('MONGODB_PORT')}/{config('MONGODB_DB_NAME')}"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_URI)
database = client[config('MONGODB_DB_NAME')]
vacancies_collection = database.get_collection("vacancies")

# helpers


def vacancy_helper(vacancy) -> dict:
    """Make dict from item for further serialization"""
    return {
        "mongo_id": str(vacancy["_id"]),
        "about_proj": vacancy["about_proj"],
        "title": vacancy["title"],
        "categories": vacancy["categories"],
        "location": vacancy["location"],
        "dou_id": vacancy["id"],
        "pub_date": vacancy["pub_date"],
    }


async def retrieve_vacancies_by_filters(categories: str, location: str, dou_id: str, pub_date: str, title: str) -> list:
    vacancies = []
    if categories or location or dou_id or pub_date or title:
        args = locals()
        args['id'] = args.pop('dou_id')  # dou_id at base is just id
        find_dict = {}
        for key, value in args.items():
            if key == 'pub_date' and value:
                find_dict[key] = re.compile(r'' + value + '-[0-9]{2}')
            elif key == 'title' and value:
                find_dict[key] = re.compile(r'' + value, re.I)
            elif value:
                find_dict[key] = {"$all": [re.compile(r'' + i, re.I) for i in value.split(" ")]}
        async for vac in vacancies_collection.find(find_dict):
            vacancies.append(vacancy_helper(vac))

    return vacancies

