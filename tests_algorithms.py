import re
import json


def check_locations(vacancies: dict, location_patterns: list):
    if not vacancies:
        return False

    for vac in vacancies:
        for loc in location_patterns:
            if re.search(loc, str(vac['location'])) is None:
                return False

    return True


def check_categories(vacancies: dict, categories: list):
    if not vacancies:
        return False

    for vac in vacancies:
        for category in categories:
            if category not in vac['categories']:
                return False

    return True


def check_fields(vacancies: dict):
    fields = ('about_proj', 'categories', 'company', 'company_url', 'id', 'location', 'offer', 'plus_skills',
              'pub_date', 'required_skills', 'salary', 'time_of_update', 'title', 'url')

    for vac in vacancies:
        for field in fields:
            if field not in vac:
                return False

    return True


def check_json_format(data: list):
    try:
        json.loads(json.dumps(data, indent=2))
        return True
    except Exception as err:
        print('Bad data format:', err)
        return False
